datalife_stock_lot_available
============================

The stock_lot_available module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_lot_available/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_lot_available)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
