============================
Stock Lot Available Scenario
============================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()


Install stock_lot_available::

    >>> config = activate_modules('stock_lot_available')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('8')
    >>> product.save()


Create lot::

    >>> Lot = Model.get('stock.lot')
    >>> lot = Lot(number='product_lot_1a', product=product)
    >>> lot.save()


Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])


Add to context locations and test_available::

    >>> config._context['locations'] = [storage_loc.id]
    >>> config._context['available_test'] = True


Check lot with context added::

    >>> lots = Lot.find([])
    >>> len(lots)
    0
    >>> config._context['locations'] = None
    >>> config._context['available_test'] = None


Create Stock Move::

    >>> StockMove = Model.get('stock.move')
    >>> move = StockMove()
    >>> move.product = product
    >>> move.lot = lot
    >>> move.uom = unit
    >>> move.quantity = 10
    >>> move.from_location = supplier_loc
    >>> move.to_location = storage_loc
    >>> move.company = company
    >>> move.unit_price = Decimal('1')
    >>> move.currency = company.currency
    >>> move.save()
    >>> move.click('assign')
    >>> move.click('do')


Add to context locations and test_available::

    >>> config._context['locations'] = [storage_loc.id]
    >>> config._context['available_test'] = True


Check lot with context added::

    >>> lots = Lot.find([])
    >>> len(lots)
    1
    >>> lots = Lot.find([('available', '=', False)])
    >>> len(lots)
    0

